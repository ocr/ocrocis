Ocrocis
===============================================================================

A project manager interface for Thomas Breuel's *Ocropy*, a toolkit for training optical character recognition (OCR) models with long short term memory networks (<https://github.com/tmbdev/ocropy>).

*Ocrocis* guides the user in training high-quality OCR models from raw PDFs or images. It features automatic project management with a minimalistic philosophy:

- Smart parameter inference, few required arguments
- Non-redundant disk usage via hardlinks (only Linux)
- Only core-Perl dependencies (aside *Ocropy*'s native dependencies')

**Downloads**

- Ocrocis: <http://cistern.cis.lmu.de/ocrocis.zip>
- Tutorial: <http://cistern.cis.lmu.de/ocrocis/tutorial.pdf>
- Tutorial data: <http://cistern.cis.lmu.de/ocrocis/tutdemo.zip>

See also the discussion forum at <https://groups.google.com/forum/#!forum/ocrocis>

---

Ocrocis (2015) is licensed by David Kaumanns (Ocrocis software) & Uwe Springmann (Ocrocis tutorial)

under Apache 2.0 (<http://www.apache.org/licenses/LICENSE-2.0>)

Center for Information and Language Processing (CIS)
University of Munich
<http://www.cis.lmu.de>


Content
-------------------------------------------------------------------------------

- [Installation](#installation)
- [How it works](#how-it-works)
- [Step-by-step example](#step-by-step-example)
- [Model application](#model-application)


Installation
-------------------------------------------------------------------------------

### Requirements

All systems:

- Manual installation: *Ocropy*, *Imagemagick*, *Perl >= 5.14*, *perl-doc*, *groff*
- **Or** automatic installation: *Docker >= 1.4.1*

*Ocrocis* can be installed via *Docker* which takes care of all dependencies, including those of *Ocropy*.

The idea of *Docker* is to deliver pre-packaged containers of software that just need to be built and run.


#### How to get Docker

##### Linux

You can use your package manager to install *docker* (or *docker.io* in Ubuntu). *Docker* is under heavy development (February 2015), so please make sure that you are installing a current version (1.5.1 as of Feb 2015).

If in doubt, install *Docker* yourself, for example on *Ubuntu*:

<http://docs.docker.com/installation/ubuntulinux/>

##### Mac OS X

Install *Xcode* (<https://itunes.apple.com/en/app/xcode/id497799835?mt=12>) and its command line tools:

    xcode-select --install

Install the *Docker* environment for *OS X* (see <https://docs.docker.com/installation/mac/>). It is recommended to use the package manager *Homebrew* (<http://brew.sh>):

    brew install Caskroom/cask/virtualbox docker boot2docker

The following two instructions are given by the *boot2docker* install routine and are repeated here for convenience. Let *launchd* start *boot2docker* at login and then load *boot2docker*:

    ln -sfv /usr/local/opt/boot2docker/*.plist ~/Library/LaunchAgents
    launchctl load ~/Library/LaunchAgents/homebrew.mxcl.boot2docker.plist

Finally, initialize, start and export *boot2docker*:

    boot2docker init
    boot2docker start
    $(boot2docker shellinit)

The last two steps, `start` and `shellinit`, have to be repeated in each new shell you open. *Ocrocis* will handle that for you, so you don't have to worry about it.

Now your *Docker* should be ready to go on *OS X*. Test it like so:

    docker run hello-world


#### How to get Ocropy for manual installation

See <https://github.com/tmbdev/ocropy>.

Note that the manual installation of *Ocropy* requires `sudo` privileges and is known to have some issues on *OS X*.


### Installing *Ocrocis*

Download and unzip *Ocrocis* from `http://cistern.cis.lmu.de/ocrocis.zip`. Then open your terminal and enter the installation directory.

Run the installation with your preferred installation method. There are two options:

1. `./install with-docker` : builds the *Docker* image and installs *Ocrocis* as *Docker+Perl* application.
2. `./install perl-only` : installs *Ocrocis* as Perl-only application. Use this method if you already have *Ocropy* installed or want to install it manually.

Notes:

- You can re-run the installation with different methods as often as you want.
- If you have run *install with-docker* several times, it is recommended to remove cached redundant *Docker* images once in a while to free space:
    - `./dockerclean`


How it works
-------------------------------------------------------------------------------

### Workflow

There are five (and a half) commands:

- **Project initialization**
    1. `ocrocis convert` - convert a PDF file or a set of images to binary PNG images
    2. `ocrocis burst` - segment each binary page image into a directory of line images
- **Iteration**
    3. `ocrocis next` - initialize the next project iteration
    4. `ocrocis train` - prepare and run training
    5. `ocrocis predict --errors` - run predictions and evaluations on annotated data
- **Model application**
    6. `ocrocis predict --dir [directory]` - run predictions on unannotated data

Each command may or may not require some preparation (e.g. cropping images or annotating lines). See the documentation for further details.

**Output flow**

*Ocrocis* itself is silent by default. All native *Ocropus* output is printed to standard output for convenient logging (e.g. with `> logfile.txt`).

Use `--verbose` to print verbose information to standard error (`2>`).


### Project architecture

**Data is structured**

*Ocrocis* works on a fixed directory structure that allows it to efficiently manage the data generated in the training process:

- book repository with pages and line images
- iterations
    - for each iteration: annotation set, annotation HTML, models
- training set
- test set

**Data is linked**

*Ocrocis* uses hardlinks to consolidate data between the book repository, annotation sets, trainings sets and test sets. Data is always linked to its source directory, never copied. This avoids redundant disk usage.

There are two kinds of source directories:

1. *book*: Original pages and line images, linked to by *annotation*, *training* and *test*
2. *annotation* (for each iteration): Original ground truths, linked to by *training* and *test*

The link structure is tree-like, analog to the directory structure, not chain-like. Each link points to the original data.

**Data is unique**

> Note: this is only valid for Linux systems. On OS X, data has to be copied due to Docker restrictions on hardlinking outside the docker container.

Changes to files (including links) anywhere in the link tree are reflected in the source directory. This is especially useful if you wish to modify the images (e.g. by cropping) at some iteration in the training process. The changes are reflected everywhere in the project.

**Data is safe**

The link structure also allows you to safely delete any link in the tree. The original data is kept, as long as there is still at least one link somewhere in the project. (Note that technically, the original data in *book* are also just links.)

This tree shows a part of an example project with two pages used for training and testing.

```
home
  ├── book
  │    ├── [original page images]
  │    ├── 0001
  │    │    └── [original line images]
  │    ├── 0002
  │    │    └── [original line images]
  │    └── ...
  │
  ├── iterations
  │    ├── 01
  │    │    ├── annotation
  │    │    │    ├── 0001
  │    │    │    │    ├── [links to line images in book]
  │    │    │    │    └── [original ground truths]
  │    │    │    ├── 0002
  │    │    │    │    ├── [links to line images in book]
  │    │    │    │    └── [original ground truths]
  │    │    │    └── ...
  │    │    ├── models
  │    │    │    └── [model files]
  │    │    └── index.html
  │    ├── 02
  │    │    └── ...
  │    └── ...
  │
  ├── training
  │    ├── 0001
  │    │    ├── [links to line images in book]
  │    │    └── [links to ground truths in annotation]
  │    └── ...
  │
  └── test
       ├── 0002
       │    ├── [links to line images in book]
       │    └── [links to ground truths in annotation]
       └── ...
```


Step-by-step example
-------------------------------------------------------------------------------

This walkthrough shows how to train models from the demo PDF.

Please refer to the `--help` page of each command for references and further options, like using a specific model or several CPUs.

Note on **Pass-through options**: Each command takes one or more pass-through options as strings. These parameters are passed through unchanged to the underlying calls of *convert* or *Ocropy*. For help on these options, please refer to their original documentations.

---

First, let's bootstrap the very first models. Create a directory named *demo* and enter it:

```
mkdir demo && cd demo
```

Download the demo PDF to the current project directory. This way you have access to it, even if you use *Docker*.

> If you run *Ocrocis* via *Docker*, the current directory is mounted into the *Docker* container. This means you only have access to files in or below the current directory, not above.

```
wget http://cistern.cis.lmu.de/ocrocis/demo.pdf
```
---

**Step 1**: Convert the demo PDF to binary page images. This populates the `book` repository with binary page images:

```
ocrocis convert --verbose --pdf demo.pdf --convert "-density 300" --ocropus-nlbin "-nochecks"
```

Convert might throw some warnings here, depending on your version. Most often you can just ignore them.

---

**Step 2**: Burst each page image into a directory of line images:

```
ocrocis burst --verbose
```

---

**Step 3**: Initialize the next iteration with a subset of the page numbers in the `book` repository. This creates the first annotation set inside the `iterations/01/annotation` directory and populates it with links to the original line images for pages 1 and 2:

```
ocrocis next --verbose 1 2
```

From these, an annotation HTML is created as `iterations/01/annotations/Correction.html`. Open it with your favourite web browser, either by double-clicking on it within a file manager or via command line, like so:

```
firefox iterations/01/annotation/Correction.html
```

> Safari on Mac OS X cannot save modifies HTML files. It is recommended to use Firefox or Chrome.

Annotate the ground truths inside the text fields, one for each line. Save the HTML file (with sources) as **the same file** via your browser's *Save as...* feature.

---

**Step 4**: Run the training on a subset of the annotation set, in this case just page 1:

```
ocrocis train --verbose --ntrain 2000 --savefreq 1000 1
```

This command extracts the ground truths from the annotation HTML and saves them alongside the line images in the annotation set. It then creates the *training* directory and populates it with links to line images and ground truths in the annotation set. It then starts training on the whole training set.

> This command automatically reuses the latest model of the previous iteration as a starting point, if it exists.

---

**Step 5**: After training, you can expand the global test set and run an evaluation, e.g. of the latest model:

```
ocrocis predict --verbose
```

This command automatically uses the remaining pages from the annotation set (here page 3). Just like `train`, it creates links in the global test set, pointing to line images and ground truths in the annotation set.

Note that the test results will be quite bad due to the tiny demo training set.

---

If you want to **continue training within the same iteration**, just run `train` again, this time with the last model as starting point:

```
ocrocis train --verbose --ntrain 2000 --savefreq 1000 --model iterations/01/models/model-00002000.pyrnn.gz 1
```

This does not make much sense for the demo PDF, but is quite useful for real life data.

Note that each time you call `train` and `test`, the annotation HTML will be parsed for any changes and the ground truths will be updated. In other words, you may change the annotation HTML whenever you want and be sure that the changes are used in all subsequent runs.

---

**Step 5.x and beyond**: Repeat **steps 3 to 5** for several iterations, until the resulting models reach the desired quality. Each iteration will reuse the latest model from the previous iteration.

---

**Step 6**: If you want to apply a model on the whole `book` repository, use the `--book` switch. This command uses the latest model of the current project:

```
ocrocis predict --book --verbose
```

You will see that your current model performs well for the trained lines and bad for the untrained test lines. To improve this model, you would need more training data.

For further information on model application, see the section below.


Model application
-------------------------------------------------------------------------------

This section explains how to apply a model on unknown (i.e. unannotated) pages and generate **only predictions** (no evaluations). Since this is a special case of testing, the `test` command is reused for this purpose.

#### Initialize new project

*Ocropus* only works on binarized line images. Therefore, you first have to initialize a new project from the new pages via `convert` and `burst`.

Run `ocrocis convert --help` and  `ocrocis burst --help` to see how to initialize a new project from PDFs or image files.

#### Apply model on project

Applying a model to a project works just like testing. Just skip the `next` and `train` commands and go right to the testing.

`ocrocis predict --book --model [FILE] [OPTIONS] [PAGE-NUMBER...]`

The `--book` switch is an alias for `--dir ./book`. It tells *Ocrocis* that you want to test on the unannotated *book* repository. Since it is unannotated, **only predictions** are computed, not evaluations.

All other options are the same as in testing (see `ocrocis predict --help`).

> If you do not specify the project home via `--home`, make sure you are within the newly initialized project directory.

> If you use *Docker*, make sure the model you want to use is in the current directory or below. Copy it there if necessary.

> If you do not specify an external model, a model from the same project is infered.

**Examples**

    ocrocis predict --book
    ocrocis predict --book {1..3}
    ocrocis predict --book --verbose --model some-model-from-another-project.pyrnn.gz
    ocrocis predict --book --verbose --model iterations/02/models
    ocrocis predict --book --verbose --model iterations/02/models/model-00010000.pyrnn.gz
    ocrocis predict --book --verbose --model iterations/02/models/model-00010000.pyrnn.gz {1..3}
