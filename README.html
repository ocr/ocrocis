<h1 id="ocrocis">Ocrocis</h1>
<p>A project manager interface for Thomas Breuel's <em>Ocropy</em>, a toolkit for training optical character recognition (OCR) models with long short term memory networks (<a href="https://github.com/tmbdev/ocropy">https://github.com/tmbdev/ocropy</a>).</p>
<p><em>Ocrocis</em> guides the user in training high-quality OCR models from raw PDFs or images. It features automatic project management with a minimalistic philosophy:</p>
<ul>
<li>Smart parameter inference, few required arguments<br /></li>
<li>Non-redundant disk usage via hardlinks<br /></li>
<li>Only core-Perl dependencies (aside <em>Ocropy</em>'s native dependencies')</li>
</ul>
<p><strong>Downloads</strong></p>
<ul>
<li>Ocrocis: <a href="http://cistern.cis.lmu.de/ocrocis.zip">http://cistern.cis.lmu.de/ocrocis.zip</a><br /></li>
<li>Tutorial: <a href="http://cistern.cis.lmu.de/ocrocis/tutorial.pdf">http://cistern.cis.lmu.de/ocrocis/tutorial.pdf</a><br /></li>
<li>Tutorial data: <a href="http://cistern.cis.lmu.de/ocrocis/tutdemo.zip">http://cistern.cis.lmu.de/ocrocis/tutdemo.zip</a></li>
</ul>
<p>See also the discussion forum at <a href="https://groups.google.com/forum/#!forum/ocrocis">https://groups.google.com/forum/#!forum/ocrocis</a></p>
<hr />
<p>Ocrocis (2015) is licensed by David Kaumanns (Ocrocis software) &amp; Uwe Springmann (Ocrocis tutorial)</p>
<p>under Apache 2.0 (<a href="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org/licenses/LICENSE-2.0</a>)</p>
<p>Center for Information and Language Processing (CIS)<br />University of Munich<br /><a href="http://www.cis.lmu.de">http://www.cis.lmu.de</a></p>
<h2 id="content">Content</h2>
<ul>
<li><a href="#installation">Installation</a><br /></li>
<li><a href="#how-it-works">How it works</a><br /></li>
<li><a href="#step-by-step-example">Step-by-step example</a><br /></li>
<li><a href="#model-application">Model application</a></li>
</ul>
<h2 id="installation">Installation</h2>
<h3 id="requirements">Requirements</h3>
<p>All systems:</p>
<ul>
<li>Manual installation: <em>Ocropy</em>, <em>Imagemagick</em>, <em>Perl &gt;= 5.14</em>, <em>perl-doc</em>, <em>groff</em><br /></li>
<li><strong>Or</strong> automatic installation: <em>Docker &gt;= 1.4.1</em></li>
</ul>
<p><em>Ocrocis</em> can be installed via <em>Docker</em> which takes care of all dependencies, including those of <em>Ocropy</em>.</p>
<p>The idea of <em>Docker</em> is to deliver pre-packaged containers of software that just need to be built and run.</p>
<h4 id="how-to-get-docker">How to get Docker</h4>
<h5 id="linux">Linux</h5>
<p>You can use your package manager to install <em>docker</em> (or <em>docker.io</em> in Ubuntu). <em>Docker</em> is under heavy development (February 2015), so please make sure that you are installing a current version (1.5.1 as of Feb 2015).</p>
<p>If in doubt, install <em>Docker</em> yourself, for example on <em>Ubuntu</em>:</p>
<p><a href="http://docs.docker.com/installation/ubuntulinux/">http://docs.docker.com/installation/ubuntulinux/</a></p>
<h5 id="mac-os-x">Mac OS X</h5>
<p>Install <em>Xcode</em> (<a href="https://itunes.apple.com/en/app/xcode/id497799835?mt=12">https://itunes.apple.com/en/app/xcode/id497799835?mt=12</a>) and its command line tools:</p>
<pre><code>xcode-select --install</code></pre>
<p>Install the <em>Docker</em> environment for <em>OS X</em> (see <a href="https://docs.docker.com/installation/mac/">https://docs.docker.com/installation/mac/</a>). It is recommended to use the package manager <em>Homebrew</em> (<a href="http://brew.sh">http://brew.sh</a>):</p>
<pre><code>brew install Caskroom/cask/virtualbox docker boot2docker</code></pre>
<p>The following two instructions are given by the <em>boot2docker</em> install routine and are repeated here for convenience. Let <em>launchd</em> start <em>boot2docker</em> at login and then load <em>boot2docker</em>:</p>
<pre><code>ln -sfv /usr/local/opt/boot2docker/*.plist ~/Library/LaunchAgents
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.boot2docker.plist</code></pre>
<p>Finally, initialize, start and export <em>boot2docker</em>:</p>
<pre><code>boot2docker init
boot2docker start
$(boot2docker shellinit)</code></pre>
<p>The last two steps, <code>start</code> and <code>shellinit</code>, have to be repeated in each new shell you open. <em>Ocrocis</em> will handle that for you, so you don't have to worry about it.</p>
<p>Now your <em>Docker</em> should be ready to go on <em>OS X</em>. Test it like so:</p>
<pre><code>docker run hello-world</code></pre>
<h4 id="how-to-get-ocropy-for-manual-installation">How to get Ocropy for manual installation</h4>
<p>See <a href="https://github.com/tmbdev/ocropy">https://github.com/tmbdev/ocropy</a>.</p>
<p>Note that the manual installation of <em>Ocropy</em> requires <code>sudo</code> privileges and is known to have some issues on <em>OS X</em>.</p>
<h3 id="installing-ocrocis">Installing <em>Ocrocis</em></h3>
<p>Download and unzip <em>Ocrocis</em> from <code>http://cistern.cis.lmu.de/ocrocis.zip</code>. Then open your terminal and enter the installation directory.</p>
<p>Run the installation with your preferred installation method. There are two options:</p>
<ol>
<li><code>./install with-docker</code> : builds the <em>Docker</em> image and installs <em>Ocrocis</em> as <em>Docker+Perl</em> application.<br /></li>
<li><code>./install perl-only</code> : installs <em>Ocrocis</em> as Perl-only application. Use this method if you already have <em>Ocropy</em> installed or want to install it manually.</li>
</ol>
<p>Notes:</p>
<ul>
<li>You can re-run the installation with different methods as often as you want.<br /></li>
<li><p>If you have run <em>install with-docker</em> several times, it is recommended to remove cached redundant <em>Docker</em> images once in a while to free space:</p>
<ul>
<li><code>./dockerclean</code></li>
</ul></li>
</ul>
<h2 id="how-it-works">How it works</h2>
<h3 id="workflow">Workflow</h3>
<p>There are five (and a half) commands:</p>
<ul>
<li><p><strong>Project initialization</strong></p>
<ol>
<li><code>ocrocis convert</code> - convert a PDF file or a set of images to binary PNG images<br /></li>
<li><code>ocrocis burst</code> - segment each binary page image into a directory of line images<br /></li>
</ol></li>
<li><p><strong>Iteration</strong></p>
<ol>
<li><code>ocrocis next</code> - initialize the next project iteration<br /></li>
<li><code>ocrocis train</code> - prepare and run training<br /></li>
<li><code>ocrocis test</code> - prepare and run tests on annotated data<br /></li>
</ol></li>
<li><p><strong>Model application</strong></p>
<ol>
<li><code>ocrocis test --book</code> - run tests on unannotated data</li>
</ol></li>
</ul>
<p>Each command may or may not require some preparation (e.g. cropping images or annotating lines). See the documentation for further details.</p>
<p><strong>Output flow</strong></p>
<p><em>Ocrocis</em> itself is silent by default. All native <em>Ocropus</em> output is printed to standard output for convenient logging (e.g. with <code>&gt; logfile.txt</code>).</p>
<p>Use <code>--verbose</code> to print verbose information to standard error (<code>2&gt;</code>).</p>
<h3 id="project-architecture">Project architecture</h3>
<p><strong>Data is structured</strong></p>
<p><em>Ocrocis</em> works on a fixed directory structure that allows it to efficiently manage the data generated in the training process:</p>
<ul>
<li>book repository with pages and line images<br /></li>
<li><p>iterations</p>
<ul>
<li>for each iteration: annotation set, annotation HTML, models<br /></li>
</ul></li>
<li>training set<br /></li>
<li><p>test set</p></li>
</ul>
<p><strong>Data is linked</strong></p>
<p><em>Ocrocis</em> uses hardlinks to consolidate data between the book repository, annotation sets, trainings sets and test sets. Data is always linked to its source directory, never copied. This avoids redundant disk usage.</p>
<p>There are two kinds of source directories:</p>
<ol>
<li><em>book</em>: Original pages and line images, linked to by <em>annotation</em>, <em>training</em> and <em>test</em><br /></li>
<li><em>annotation</em> (for each iteration): Original ground truths, linked to by <em>training</em> and <em>test</em></li>
</ol>
<p>The link structure is tree-like, analog to the directory structure, not chain-like. Each link points to the original data.</p>
<p><strong>Data is unique</strong></p>
<p>Changes to files (including links) anywhere in the link tree are reflected in the source directory. This is especially useful if you wish to modify the images (e.g. by cropping) at some iteration in the training process. The changes are reflected everywhere in the project.</p>
<p><strong>Data is safe</strong></p>
<p>The link structure also allows you to safely delete any link in the tree. The original data is kept, as long as there is still at least one link somewhere in the project. (Note that technically, the original data in <em>book</em> are also just links.)</p>
<p>This tree shows a part of an example project with two pages used for training and testing.</p>
<pre><code>home
  ├── book
  │    ├── [original page images]
  │    ├── 0001
  │    │    └── [original line images]
  │    ├── 0002
  │    │    └── [original line images]
  │    └── ...
  │
  ├── iterations
  │    ├── 01
  │    │    ├── annotation
  │    │    │    ├── 0001
  │    │    │    │    ├── [links to line images in book]
  │    │    │    │    └── [original ground truths]
  │    │    │    ├── 0002
  │    │    │    │    ├── [links to line images in book]
  │    │    │    │    └── [original ground truths]
  │    │    │    └── ...
  │    │    ├── models
  │    │    │    └── [model files]
  │    │    └── index.html
  │    ├── 02
  │    │    └── ...
  │    └── ...
  │
  ├── training
  │    ├── 0001
  │    │    ├── [links to line images in book]
  │    │    └── [links to ground truths in annotation]
  │    └── ...
  │
  └── test
       ├── 0002
       │    ├── [links to line images in book]
       │    └── [links to ground truths in annotation]
       └── ...</code></pre>
<h2 id="step-by-step-example">Step-by-step example</h2>
<p>This walkthrough shows how to train models from the demo PDF.</p>
<p>Please refer to the <code>--help</code> page of each command for references and further options, like using a specific model or several CPUs.</p>
<p>Note on <strong>Pass-through options</strong>: Each command takes one or more pass-through options as strings. These parameters are passed through unchanged to the underlying calls of <em>convert</em> or <em>Ocropy</em>. For help on these options, please refer to their original documentations.</p>
<hr />
<p>First, let's bootstrap the very first models. Create a directory named <em>demo</em> and enter it:</p>
<pre><code>mkdir demo &amp;&amp; cd demo</code></pre>
<p>Download the demo PDF to the current project directory. This way you have access to it, even if you use <em>Docker</em>.</p>
<blockquote>
<p>If you run <em>Ocrocis</em> via <em>Docker</em>, the current directory is mounted into the <em>Docker</em> container. This means you only have access to files in or below the current directory, not above.</p>
</blockquote>
<pre><code>wget http://cistern.cis.lmu.de/ocrocis/demo.pdf</code></pre>
<hr />
<p><strong>Step 1</strong>: Convert the demo PDF to binary page images. This populates the <code>book</code> repository with binary page images:</p>
<pre><code>ocrocis convert --verbose --pdf demo.pdf --convert &quot;-density 300&quot; --ocropus-nlbin &quot;-nochecks&quot;</code></pre>
<p>Convert might throw some warnings here, depending on your version. Most often you can just ignore them.</p>
<hr />
<p><strong>Step 2</strong>: Burst each page image into a directory of line images:</p>
<pre><code>ocrocis burst</code></pre>
<hr />
<p><strong>Step 3</strong>: Initialize the next iteration with a subset of the page numbers in the <code>book</code> repository. This creates the first annotation set inside the <code>iterations/01/annotation</code> directory and populates it with links to the original line images for pages 1 and 2:</p>
<pre><code>ocrocis next --verbose 1 2</code></pre>
<p>From these, an annotation HTML is created as <code>iterations/01/annotations/Correction.html</code>. Open it with your favourite web browser, either by double-clicking on it within a file manager or via command line, like so:</p>
<pre><code>firefox iterations/01/annotation/Correction.html</code></pre>
<blockquote>
<p>Safari on Mac OS X cannot save modifies HTML files. It is recommended to use Firefox or Chrome.</p>
</blockquote>
<p>Annotate the ground truths inside the text fields, one for each line. Save the HTML file (with sources) as <strong>the same file</strong> via your browser's <em>Save as...</em> feature.</p>
<hr />
<p><strong>Step 4</strong>: Run the training on a subset of the annotation set, in this case just page 1:</p>
<pre><code>ocrocis train --verbose --ntrain 2000 --savefreq 1000 1</code></pre>
<p>This command extracts the ground truths from the annotation HTML and saves them alongside the line images in the annotation set. It then creates the <em>training</em> directory and populates it with links to line images and ground truths in the annotation set. It then starts training on the whole training set.</p>
<blockquote>
<p>This command automatically reuses the latest model of the previous iteration as a starting point, if it exists.</p>
</blockquote>
<hr />
<p><strong>Step 5</strong>: After training, you can expand the global test set and run an evaluation, e.g. of the latest model:</p>
<pre><code>ocrocis test --verbose</code></pre>
<p>This command automatically uses the remaining pages from the annotation set (here page 3). Just like <code>train</code>, it creates links in the global test set, pointing to line images and ground truths in the annotation set.</p>
<p>Note that the test results will be quite bad due to the tiny demo training set.</p>
<hr />
<p>If you want to <strong>continue training within the same iteration</strong>, just run <code>train</code> again, this time with the last model as starting point:</p>
<pre><code>ocrocis train --verbose --ntrain 2000 --savefreq 1000 --model iterations/01/models/model-00002000.pyrnn.gz 1</code></pre>
<p>This does not make much sense for the demo PDF, but is quite useful for real life data.</p>
<p>Note that each time you call <code>train</code> and <code>test</code>, the annotation HTML will be parsed for any changes and the ground truths will be updated. In other words, you may change the annotation HTML whenever you want and be sure that the changes are used in all subsequent runs.</p>
<hr />
<p><strong>Step 5.x and beyond</strong>: Repeat <strong>steps 3 to 5</strong> for several iterations, until the resulting models reach the desired quality. Each iteration will reuse the latest model from the previous iteration.</p>
<hr />
<p><strong>Step 6</strong>: If you want to apply a model on the whole <code>book</code> repository, use the <code>--book</code> switch. This command uses the latest model of the current project:</p>
<pre><code>ocrocis test --book --verbose</code></pre>
<p>You will see that your current model performs well for the trained lines and bad for the untrained test lines. To improve this model, you would need more training data.</p>
<p>For further information on model application, see the section below.</p>
<h2 id="model-application">Model application</h2>
<p>This section explains how to apply a model on unknown (i.e. unannotated) pages and generate <strong>only predictions</strong> (no evaluations). Since this is a special case of testing, the <code>test</code> command is reused for this purpose.</p>
<h4 id="initialize-new-project">Initialize new project</h4>
<p><em>Ocropus</em> only works on binarized line images. Therefore, you first have to initialize a new project from the new pages via <code>convert</code> and <code>burst</code>.</p>
<p>Run <code>ocrocis convert --help</code> and <code>ocrocis burst --help</code> to see how to initialize a new project from PDFs or image files.</p>
<h4 id="apply-model-on-project">Apply model on project</h4>
<p>Applying a model to a project works just like testing. Just skip the <code>next</code> and <code>train</code> commands and go right to the testing.</p>
<p><code>ocrocis test --book --model [FILE] [OPTIONS] [PAGE-NUMBER...]</code></p>
<p>The <code>--book</code> switch tells <em>Ocrocis</em> that you want to test on the unannotated <em>book</em> repository. Since it is unannotated, <strong>only predictions</strong> are computed, not evaluations.</p>
<p>All other options are the same as in testing (see <code>ocrocis test --help</code>).</p>
<blockquote>
<p>If you do not specify the project home via <code>--home</code>, make sure you are within the newly initialized project directory.</p>
</blockquote>
<blockquote>
<p>If you use <em>Docker</em>, make sure the model you want to use is in the current directory or below. Copy it there if necessary.</p>
</blockquote>
<blockquote>
<p>If you do not specify an external model, a model from the same project is infered.</p>
</blockquote>
<p><strong>Examples</strong></p>
<pre><code>ocrocis test --book
ocrocis test --book {1..3}
ocrocis test --book --verbose --model some-model-from-another-project.pyrnn.gz
ocrocis test --book --verbose --model iterations/02/models
ocrocis test --book --verbose --model iterations/02/models/model-00010000.pyrnn.gz
ocrocis test --book --verbose --model iterations/02/models/model-00010000.pyrnn.gz {1..3}</code></pre>
